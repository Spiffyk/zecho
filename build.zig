const std = @import("std");
const cmake = @import("cmake.zig");

pub fn build(b: *std.Build) void
{
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const libuv = cmake.addProject(b, .{
        .name = "libuv",
        .path = "vendor/libuv",
        .optimize = optimize,
        .cmake_args = &.{
            "-DLIBUV_BUILD_SHARED=OFF",
        },
    });

    const ngtcp2 = cmake.addProject(b, .{
        .name = "ngtcp2",
        .path = "vendor/ngtcp2",
        .optimize = optimize,
        .cmake_args = &.{
            "-DENABLE_OPENSSL=OFF",
            "-DENABLE_GNUTLS=ON",
            "-DENABLE_SHARED_LIB=OFF",
            "-DENABLE_STATIC_LIB=ON",
        },
    });

    const exe = b.addExecutable(.{
        .name = "zecho",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    exe.linkLibC(); // we have to because of dependencies :-(
    exe.linkSystemLibrary("gnutls");
    exe.addObjectFile(libuv.buildPath("libuv.a"));
    exe.addSystemIncludePath(libuv.projectPath("include"));
    exe.addObjectFile(ngtcp2.buildPath("lib/libngtcp2.a"));
    exe.addSystemIncludePath(ngtcp2.buildPath("lib/includes"));
    exe.addSystemIncludePath(ngtcp2.projectPath("lib/includes"));

    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());

    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
