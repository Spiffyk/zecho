//! Support for building Zig projects that use vendored CMake libraries.

const std = @import("std");

const Child = std.process.Child;
const LazyPath = std.Build.LazyPath;
const Step = std.Build.Step;

pub const CMakeOptions = struct {
    /// Name of the CMake project. Must consist of filename-safe characters.
    name: []const u8,
    /// Path to the CMake project, relative to the build root.
    path: []const u8,
    /// Optimization mode for the CMake project. The moduel translates this
    /// roughly into CMake's standard build types.
    optimize: std.builtin.Mode,
    /// Additional project-specific arguments for CMake. Use this to set your
    /// `-D<var>=<value>` project options.
    cmake_args: []const []const u8 = &.{},
};

/// Represents a [Step] that builds a CMake project.
pub const CMakeStep = struct {
    step: Step,
    path: []const u8,
    optimize: std.builtin.Mode,
    cmake_args: []const []const u8,
    built_files: std.StringArrayHashMap(std.Build.GeneratedFile),

    /// Resolves `path` relatively to CMake's build directory. Use this to
    /// retrieve build artifacts, like libraries.
    pub fn buildPath(self: *CMakeStep, path: []const u8) LazyPath
    {
        const entry = self.built_files.getOrPutValue(
                path, .{ .step = &self.step })
            catch @panic("OOM");
        return .{ .generated = entry.value_ptr };
    }

    /// Resolves `path` relatively to the project directory. Use this to
    /// retrieve library include paths.
    pub fn projectPath(self: *CMakeStep, path: []const u8) LazyPath
    {
        const b = self.step.owner;
        return .{ .path = b.pathJoin(&.{ self.path, path }) };
    }
};


/// Adds a new CMake step to the build.
pub fn addProject(b: *std.Build, opts: CMakeOptions) *CMakeStep
{
    var cms = b.allocator.create(CMakeStep) catch @panic("OOM");
    cms.* = CMakeStep{
        .step = Step.init(.{
            .id = .custom,
            .name = opts.name,
            .owner = b,
            .makeFn = makeCMakeFn,
        }),
        .path = opts.path,
        .optimize = opts.optimize,
        .cmake_args = b.dupeStrings(opts.cmake_args),
        .built_files =
            std.StringArrayHashMap(std.Build.GeneratedFile).init(b.allocator),
    };
    return cms;
}


fn makeCMakeFn(step: *Step, prog_node: *std.Progress.Node) anyerror!void
{
    _ = prog_node;
    const self = @fieldParentPtr(CMakeStep, "step", step);
    const b = self.step.owner;

    const project_path = b.pathFromRoot(self.path);
    const build_type_name = switch (self.optimize) {
        .Debug => "Debug",
        .ReleaseSafe => "Release",
        .ReleaseFast => "Release",
        .ReleaseSmall => "MinSizeRel",
    };


    // Generate CMake build directory //

    var gen_man = b.cache.obtain();
    defer gen_man.deinit();

    gen_man.hash.addBytes(self.step.name);
    gen_man.hash.addBytes(self.path);
    gen_man.hash.addBytes(build_type_name);
    gen_man.hash.addListOfBytes(self.cmake_args);

    const gen_cache_hit = try step.cacheHit(&gen_man);
    const gen_digest = gen_man.final();
    const build_path = try b.cache_root.join(b.allocator, &.{
        "cmake", &gen_digest, b.fmt("build_{s}", .{self.step.name})
    });

    if (!gen_cache_hit) {
        var gen_argv = std.ArrayList([]const u8).init(b.allocator);
        defer gen_argv.deinit();
        try gen_argv.appendSlice(&.{
            "cmake",
            "-B", build_path,
            "-S", project_path,
            b.fmt("-DCMAKE_BUILD_TYPE={s}", .{build_type_name}),
            "-DCMAKE_C_COMPILER='zig;cc'",
            "-DCMAKE_CXX_COMPILER='zig;c++'",
        });
        try gen_argv.appendSlice(self.cmake_args);

        const gen_result = try Child.exec(.{
            .argv = gen_argv.items,
            .allocator = b.allocator,
        });
        if (gen_result.term != .Exited)
            return error.ChildNotExited;
        if (gen_result.term.Exited != 0) {
            std.log.err("Could not generate CMake build for '{s}': {s}",
                    .{self.step.name, gen_result.stderr});
            return error.ChildExitedWithNonZero;
        }
    }


    // Build CMake project //

    var build_result = try Child.exec(.{
        .argv = &.{"cmake", "--build", build_path, "-j"},
        .allocator = b.allocator
    });
    if (build_result.term != .Exited)
        return error.ChildNotExited;
    if (build_result.term.Exited != 0) {
        std.log.err("Could not generate CMake build for '{s}': {s}",
                .{self.step.name, build_result.stderr});
        return error.ChildExitedWithNonZero;
    }

    var it = self.built_files.iterator();
    while (it.next()) |e| {
        e.value_ptr.path = b.pathJoin(&.{build_path, e.key_ptr.*});
    }
}
