const std = @import("std");

pub inline fn errPanic(err: anyerror) noreturn
{
    std.debug.panic("error: {}", .{err});
}

pub fn TypeOfStructField(comptime T: type, comptime fieldName: []const u8) type
{
    const t = @typeInfo(T);
    if (t != .Struct)
        @compileError(T ++ " is not a struct");

    for (t.Struct.fields) |field| {
        if (std.mem.eql(u8, field.name, fieldName))
            return field.type;
    }
    @compileError(T ++ " has no field named '" ++ fieldName ++ "'");
}

pub fn UInt(comptime bits: u16) type
{
    return @Type(std.builtin.Type{
        .Int = .{
            .signedness = .unsigned,
            .bits = bits,
        }
    });
}
