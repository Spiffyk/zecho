const std = @import("std");
const c = @cImport({
    @cInclude("uv.h");
});

const util = @import("util.zig");


// Utilities ///////////////////////////////////////////////////////////////////

inline fn uvc(errno: c_int) Error!void
{
    return uvError(errno) orelse void{};
}

inline fn initUvHandle(wrapper: anytype,
                loop: *Loop,
                comptime init_func: anytype)
    Error!void
{
    try uvc(init_func(&loop.loop, &wrapper.handle));
    wrapper.handle.data = wrapper;
}

inline fn wrapSimpleCb(comptime T: type,
                       comptime UVT: type,
                       comptime opt_cb: ?*const fn(*T) void)
    ?*const fn([*c]UVT) callconv(.C) void
{
    return struct {
        fn actualCb(hnd: [*c]UVT) callconv(.C) void
        {
            var t: *T = @fieldParentPtr(T, "handle",
                    @as(*util.TypeOfStructField(T, "handle"), @ptrCast(hnd)));
            if (opt_cb) |cb| {
                cb(t);
            }
        }
    }.actualCb;
}

inline fn wrapStatusCb(comptime T: type,
                       comptime UVT: type,
                       comptime opt_cb: ?*const fn(*T, ?Error) void)
    ?*const fn([*c]UVT, c_int) callconv(.C) void
{
    return struct {
        fn actualCb(hnd: [*c]UVT, status: c_int) callconv(.C) void
        {
            var t: *T = @fieldParentPtr(T, "handle",
                    @as(*util.TypeOfStructField(T, "handle"), @ptrCast(hnd)));
            if (opt_cb) |cb| {
                cb(t, if (status == 0) null else uvError(status));
            }
        }
    }.actualCb;
}

inline fn wrapAllocCb(comptime T: type,
                      comptime cb: AllocCb(T))
    ?* const fn([*c]c.uv_handle_t, usize, [*c]c.uv_buf_t) callconv(.C) void
{
    return struct {
        fn actualCb(hnd: [*c]c.uv_handle_t, suggested_size: usize,
                    buf: [*c]c.uv_buf_t) callconv(.C) void
        {
            var t: *T = @fieldParentPtr(T, "handle",
                    @as(*util.TypeOfStructField(T, "handle"), @ptrCast(hnd)));
            const zbuf = cb(t, suggested_size);
            buf.*.base = @ptrCast(zbuf);
            buf.*.len = zbuf.len;
        }
    }.actualCb;
}

fn slicesToBufs(bufs: []const []const u8, uv_bufs: []c.uv_buf_t)
    []const c.uv_buf_t
{
    std.debug.assert(bufs.len == uv_bufs.len);
    for (bufs, 0..) |buf, i| {
        uv_bufs[i] = .{
            .base = @constCast(@ptrCast(buf)),
            .len = buf.len,
        };
    }
    return uv_bufs[0..bufs.len];
}


// LibUV handles and other types ///////////////////////////////////////////////

//pub const AllocCb = *const fn()
pub fn AllocCb(comptime T: type) type
{
    return *const fn(handle: *T, suggested_size: usize) []u8;
}

pub const Loop = struct {
    loop: c.uv_loop_t = undefined,

    pub fn init(self: *Loop) Error!void
    {
        try uvc(c.uv_loop_init(&self.loop));
        self.loop.data = self;
    }

    pub fn close(self: *Loop) Error!void
    {
        try uvc(c.uv_loop_close(&self.loop));
    }

    pub fn run(self: *Loop, mode: enum {Default, Once, NoWait}) Error!void
    {
        try uvc(c.uv_run(&self.loop, switch (mode) {
            .Default => c.UV_RUN_DEFAULT,
            .Once => c.UV_RUN_ONCE,
            .NoWait => c.UV_RUN_NOWAIT,
        }));
    }
};

pub const Timer = struct {
    handle: c.uv_timer_t = undefined,

    pub fn init(self: *Timer, loop: *Loop) Error!void
    {
        try initUvHandle(self, loop, c.uv_timer_init);
    }

    pub inline fn close(self: *Timer, comptime cb: ?*const fn(*Timer) void) void
    {
        c.uv_close(@ptrCast(&self.handle),
                wrapSimpleCb(@This(), c.uv_handle_t, cb));
    }

    pub inline fn start(self: *Timer, comptime cb: ?*const fn(*Timer) void,
                        timeout: u64, repeat: u64)
        Error!void
    {
        try uvc(c.uv_timer_start(@ptrCast(&self.handle),
                wrapSimpleCb(@This(), @TypeOf(self.handle), cb), timeout, repeat));
    }

    pub inline fn stop(self: *Timer) Error!void
    {
        try uvc(c.uv_timer_stop(@ptrCast(&self.handle)));
    }

    pub inline fn again(self: *Timer) Error!void
    {
        try uvc(c.uv_timer_again(@ptrCast(&self.handle)));
    }

    pub inline fn setRepeat(self: *Timer, repeat: u64) void
    {
        c.uv_timer_set_repeat(@ptrCast(&self.handle), repeat);
    }

    pub inline fn getRepeat(self: *Timer) u64
    {
        return c.uv_timer_get_repeat(@ptrCast(&self.handle));
    }
};

pub const Udp = struct {
    handle: c.uv_udp_t = undefined,
    arena: std.heap.ArenaAllocator,

    pub const Flags = packed struct(c_uint) {
        ipv6Only: bool = false,
        partial: bool = false,
        reuseAddr: bool = false,
        mmsgChunk: bool = false,
        mmsgFree: bool = false,
        linuxRecvErr: bool = false,
        _padding1: util.UInt(2) = 0,
        recvMmsg: bool = false,
        _padding2: util.UInt(@bitSizeOf(c_uint) - 9) = 0,


        inline fn toC(self: *const Flags) c_uint
        {
            return @bitCast(self.*);
        }

        inline fn fromC(flags: c_uint) Flags
        {
            return @bitCast(flags);
        }
    };

    pub const RecvCb = *const fn(
            udp: *Udp, nread: isize, buf: []const u8,
            addr: ?*const std.net.Address, flags: Flags) void;


    pub inline fn init(self: *Udp, loop: *Loop, allocator: std.mem.Allocator)
        Error!void
    {
        try initUvHandle(self, loop, c.uv_udp_init);
        self.arena = std.heap.ArenaAllocator.init(allocator);
    }

    pub inline fn close(self: *Udp, comptime cb: ?*const fn(*Udp) void) void
    {
        c.uv_close(@ptrCast(&self.handle),
                wrapSimpleCb(@This(), c.uv_handle_t, cb));
        self.arena.deinit();
    }

    pub inline fn bind(self: *Udp, address: *const std.net.Address,
                       flags: Flags)
        Error!void
    {
        try uvc(c.uv_udp_bind(&self.handle, @ptrCast(address), flags.toC()));
    }

    pub const Send = struct {
        handle: c.uv_udp_send_t = undefined,

        pub inline fn udp(self: *Send) *Udp
        {
            return @fieldParentPtr(Udp, "handle", self.handle.handle);
        }
    };

    pub const SendCb = *const fn(*Send, ?Error) void;

    pub inline fn send(self: *Udp, req: *Send, bufs: []const []const u8,
                       addr: ?*const std.net.Address, comptime send_cb: ?SendCb)
        !void
    {
        const a = self.arena.allocator();
        const uv_bufs_buf = try a.alloc(c.uv_buf_t, bufs.len);
        defer a.free(uv_bufs_buf); // uv_udp_send makes its copy of the list
        const uv_bufs = slicesToBufs(bufs, uv_bufs_buf);
        try uvc(c.uv_udp_send(&req.handle, &self.handle, @ptrCast(uv_bufs),
                    @intCast(uv_bufs.len), @ptrCast(addr),
                    wrapStatusCb(Send, c.uv_udp_send_t, send_cb)));
    }

    pub inline fn trySend(self: *Udp, bufs: []const []const u8,
                          addr: ?*std.net.Address) !void
    {
        const a = self.arena.allocator();
        const uv_bufs_buf = try a.alloc(c.uv_buf_t, bufs.len);
        defer a.free(uv_bufs_buf);
        const uv_bufs = slicesToBufs(bufs, uv_bufs_buf);
        try uvc(c.uv_udp_try_send(&self.handle, @ptrCast(uv_bufs),
                    @intCast(uv_bufs.len), @ptrCast(addr)));
    }

    pub inline fn recvStart(self: *Udp,
                            comptime alloc_cb: AllocCb(Udp),
                            comptime recv_cb: RecvCb)
        Error!void
    {
        const uv_recv_cb = struct {
            fn actualCb(hnd: [*c]c.uv_udp_t, nread: isize,
                        buf: [*c]const c.uv_buf_t,
                        addr: [*c]const c.struct_sockaddr,
                        flags: c_uint) callconv(.C) void
            {
                var t: *Udp = @fieldParentPtr(Udp, "handle",
                        @as(*c.uv_udp_t, @ptrCast(hnd)));
                recv_cb(t, nread, buf.*.base[0..buf.*.len], @ptrCast(@alignCast(addr)),
                        Flags.fromC(flags));
            }
        }.actualCb;

        try uvc(c.uv_udp_recv_start(&self.handle, wrapAllocCb(Udp, alloc_cb),
                    uv_recv_cb));
    }
};


// LibUV Errors ////////////////////////////////////////////////////////////////

pub const Error = error {
    TooBig,
    Access,
    AddrInUse,
    AddrNotAvail,
    AfNoSupport,
    Again,
    AiAddrFamily,
    AiAgain,
    AiBadFlags,
    AiBadHints,
    AiCanceled,
    AiFail,
    AiFamily,
    AiMemory,
    AiNoData,
    AiNoName,
    AiOverflow,
    AiProtocol,
    AiService,
    AiSockType,
    Already,
    BadF,
    Busy,
    Canceled,
    Charset,
    ConnAborted,
    ConnRefused,
    ConnReset,
    DestAddrReq,
    Exists,
    Fault,
    FBig,
    HostUnreach,
    Intr,
    InvalidArgument,
    Io,
    IsConn,
    IsDir,
    Loop,
    MFile,
    MsgSize,
    NameTooLong,
    NetDown,
    NetUnreach,
    NFile,
    NoBufS,
    NoDev,
    NoEnt,
    NoMem,
    NoNet,
    NoProtoOpt,
    NoSpc,
    NoSys,
    NotConn,
    NotDir,
    NotEmpty,
    NotSock,
    NotSup,
    Overflow,
    Perm,
    Pipe,
    Proto,
    ProtoNoSupport,
    ProtoType,
    Range,
    RoFs,
    Shutdown,
    SPipe,
    Srch,
    TimedOut,
    TxtBsy,
    XDev,
    Eof,
    NxIo,
    MLink,
    HostDown,
    RemoteIo,
    NoTty,
    FType,
    IlSeq,
    SockTNoSupport,
    NoData,
    Unatch,

    Unknown,
};

fn uvError(uv_errno: c_int) ?Error
{
    return switch (uv_errno) {
        0 => null,
        c.UV_E2BIG => Error.TooBig,
        c.UV_EACCES => Error.Access,
        c.UV_EADDRINUSE => Error.AddrInUse,
        c.UV_EADDRNOTAVAIL => Error.AddrNotAvail,
        c.UV_EAFNOSUPPORT => Error.AfNoSupport,
        c.UV_EAGAIN => Error.Again,
        c.UV_EAI_ADDRFAMILY => Error.AiAddrFamily,
        c.UV_EAI_AGAIN => Error.AiAgain,
        c.UV_EAI_BADFLAGS => Error.AiBadFlags,
        c.UV_EAI_BADHINTS => Error.AiBadHints,
        c.UV_EAI_CANCELED => Error.AiCanceled,
        c.UV_EAI_FAIL => Error.AiFail,
        c.UV_EAI_FAMILY => Error.AiFamily,
        c.UV_EAI_MEMORY => Error.AiMemory,
        c.UV_EAI_NODATA => Error.AiNoData,
        c.UV_EAI_NONAME => Error.AiNoName,
        c.UV_EAI_PROTOCOL => Error.AiProtocol,
        c.UV_EAI_SERVICE => Error.AiService,
        c.UV_EAI_SOCKTYPE => Error.AiSockType,
        c.UV_EALREADY => Error.Already,
        c.UV_EBADF => Error.BadF,
        c.UV_EBUSY => Error.Busy,
        c.UV_ECANCELED => Error.Canceled,
        c.UV_ECHARSET => Error.Charset,
        c.UV_ECONNABORTED => Error.ConnAborted,
        c.UV_ECONNREFUSED => Error.ConnRefused,
        c.UV_ECONNRESET => Error.ConnReset,
        c.UV_EDESTADDRREQ => Error.DestAddrReq,
        c.UV_EEXIST => Error.Exists,
        c.UV_EFAULT => Error.Fault,
        c.UV_EFBIG => Error.FBig,
        c.UV_EHOSTUNREACH => Error.HostUnreach,
        c.UV_EINTR => Error.Intr,
        c.UV_EINVAL => Error.InvalidArgument,
        c.UV_EIO => Error.Io,
        c.UV_EISCONN => Error.IsConn,
        c.UV_EISDIR => Error.IsDir,
        c.UV_ELOOP => Error.Loop,
        c.UV_EMFILE => Error.MFile,
        c.UV_EMSGSIZE => Error.MsgSize,
        c.UV_ENAMETOOLONG => Error.NameTooLong,
        c.UV_ENETDOWN => Error.NetDown,
        c.UV_ENETUNREACH => Error.NetUnreach,
        c.UV_ENFILE => Error.NFile,
        c.UV_ENOBUFS => Error.NoBufS,
        c.UV_ENODEV => Error.NoDev,
        c.UV_ENOENT => Error.NoEnt,
        c.UV_ENOMEM => Error.NoMem,
        c.UV_ENONET => Error.NoNet,
        c.UV_ENOPROTOOPT => Error.NoProtoOpt,
        c.UV_ENOSPC => Error.NoSpc,
        c.UV_ENOSYS => Error.NoSys,
        c.UV_ENOTCONN => Error.NotConn,
        c.UV_ENOTDIR => Error.NotDir,
        c.UV_ENOTEMPTY => Error.NotEmpty,
        c.UV_ENOTSOCK => Error.NotSock,
        c.UV_ENOTSUP => Error.NotSup,
        c.UV_EOVERFLOW => Error.Overflow,
        c.UV_EPERM => Error.Perm,
        c.UV_EPIPE => Error.Pipe,
        c.UV_EPROTO => Error.Proto,
        c.UV_EPROTONOSUPPORT => Error.ProtoNoSupport,
        c.UV_EPROTOTYPE => Error.ProtoType,
        c.UV_ERANGE => Error.Range,
        c.UV_EROFS => Error.RoFs,
        c.UV_ESHUTDOWN => Error.Shutdown,
        c.UV_ESPIPE => Error.SPipe,
        c.UV_ESRCH => Error.Srch,
        c.UV_ETIMEDOUT => Error.TimedOut,
        c.UV_ETXTBSY => Error.TxtBsy,
        c.UV_EXDEV => Error.XDev,
        c.UV_EOF => Error.Eof,
        c.UV_ENXIO => Error.NxIo,
        c.UV_EMLINK => Error.MLink,
        c.UV_EHOSTDOWN => Error.HostDown,
        c.UV_EREMOTEIO => Error.RemoteIo,
        c.UV_ENOTTY => Error.NoTty,
        c.UV_EFTYPE => Error.FType,
        c.UV_EILSEQ => Error.IlSeq,
        c.UV_ESOCKTNOSUPPORT => Error.SockTNoSupport,
        c.UV_ENODATA => Error.NoData,
        c.UV_EUNATCH => Error.Unatch,

        else => Error.Unknown,
    };
}
