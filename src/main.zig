/// This is the global config.
///
/// TODO: this is hardcoded for now, but I definitely want to make a file out of
/// this, or something.
const config: ZechoConfig = .{

};



const std = @import("std");
const log = std.log;

const util = @import("util.zig");
const uv = @import("uv.zig");

const panic = std.debug.panic;
const errPanic = util.errPanic;



const Protocol = enum {
    udp,
    tcp,
    tls,
    quic,
};

const ProtocolConfig = union(Protocol) {
    udp: void,
    tcp: void,
    tls: struct {
        cert: []const u8,
        key: []const u8,
        allow_resumption: bool = true,
    },
    quic: struct {
        cert: []const u8,
        key: []const u8,
        allow_0rtt: bool = true,
    },
};

const ListenerConfig = struct {
    addr: []const u8,
    port: u16,
    protocol: ProtocolConfig,
};

const LogGroup = enum(u8) {
    info,
    warning,
    connection,
    echo,
    ngtcp2_debug,
};

const ZechoConfig = struct {
    listeners: []ListenerConfig = .{},
    log_groups: std.EnumSet(LogGroup) =
        std.EnumSet(LogGroup).initMany(.{ .infos, .warning }),
};


const TimerWithCtx = struct {
    timer: uv.Timer = undefined,
    counter: u64 = 0,

    pub fn start(self: *TimerWithCtx, loop: *uv.Loop) uv.Error!void
    {
        self.* = .{};
        try self.timer.init(loop);
        try self.timer.start(TimerWithCtx.cb, 1000, 500);
    }

    pub fn close(self: *TimerWithCtx) void
    {
        self.timer.close(null);
    }

    fn cb(t: *uv.Timer) void {
        var self = @fieldParentPtr(TimerWithCtx, "timer", t);
        std.debug.print("Timer {}\n", .{self.counter});
        self.counter += 1;
    }
};

const UdpEcho = struct {
    const BUF_SIZE = 65536;

    udp: uv.Udp = undefined,
    bufpool: std.heap.MemoryPool([BUF_SIZE]u8),
    sendpool: std.heap.MemoryPool(EchoSend),

    const Options = struct {
        loop: *uv.Loop,
        allocator: std.mem.Allocator,
        ip: []const u8 = "::1",
        port: u16 = 5553,
    };

    const EchoSend = struct {
        req: uv.Udp.Send = undefined,
        buf: *[BUF_SIZE]u8,
    };

    pub fn start(self: *UdpEcho, opts: *const Options) !void
    {
        self.* = .{
            .bufpool = try @TypeOf(self.bufpool).initPreheated(opts.allocator, 32),
            .sendpool = try @TypeOf(self.sendpool).initPreheated(opts.allocator, 32),
        };

        try self.udp.init(opts.loop, opts.allocator);
        errdefer self.udp.close(null);
        try self.udp.bind(&try std.net.Address.resolveIp(opts.ip, opts.port), .{
            .reuseAddr = true,
        });
        try self.udp.recvStart(allocCb, recvCb);
    }

    pub fn close(self: *UdpEcho) void
    {
        self.bufpool.deinit();
        self.sendpool.deinit();
        self.udp.close(null);
    }

    fn allocCb(udp: *uv.Udp, suggested_size: usize) []u8
    {
        _ = suggested_size;
        var self = @fieldParentPtr(@This(), "udp", udp);
        return self.bufpool.create() catch @panic("OOM");
    }

    fn recvCb(udp: *uv.Udp, nread: isize, buf: []const u8,
              addr: ?*const std.net.Address, flags: uv.Udp.Flags) void
    {
        _ = flags;
        var self = @fieldParentPtr(@This(), "udp", udp);
        std.debug.assert(buf.len == BUF_SIZE);

        if (nread == 0)
            return;

        std.debug.print("Received {} bytes from {any}\n", .{nread, addr});

        var send = self.sendpool.create() catch @panic("OOM");
        send.* = .{ .buf = @constCast(@ptrCast(buf)) };
        self.udp.send(&send.req, &.{ buf[0..@bitCast(nread)] }, addr, sendCb)
            catch |err| util.errPanic(err);
    }

    fn sendCb(req: *uv.Udp.Send, err: ?uv.Error) void
    {
        if (err) |nnerr|
            std.debug.print("send error: {}", .{nnerr});

        var send = @fieldParentPtr(EchoSend, "req", req);
        var self = @fieldParentPtr(UdpEcho, "udp", send.req.udp());
        self.bufpool.destroy(@alignCast(send.buf));
        self.sendpool.destroy(send);
    }
};



pub fn main() !void {
    //var gpa = std.heap.GeneralPurposeAllocator(.{}){};

    var loop: uv.Loop = undefined;
    try loop.init();
    defer loop.close() catch |err| errPanic(err);

    // testing timer
    //var t: TimerWithCtx = undefined;
    //try t.start(&loop);
    //defer t.close();

    // UDP
    var udp_echo: UdpEcho = undefined;
    try udp_echo.start(&.{
        .loop = &loop,
        .allocator = std.heap.page_allocator,
    });
    defer udp_echo.close();

    try loop.run(.Default);
}
